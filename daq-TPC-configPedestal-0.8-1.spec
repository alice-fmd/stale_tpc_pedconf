Summary: A program to configure the ALTRO pedestal memory via the DDL.
Name: daq-TPC-configPedestal
Version: 0.8
Release: 1
License: CERN/PH-ED
Group: Applications/Alice
Source0: %{name}-%{version}.src.tar.gz
Packager: Christian Lippmann <Christian.Lippmann@cern.ch>
URL: https://subversion.gsi.de/alice/tpc-fee/TPCconfigPed/trunk/
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires: date
Requires: date

%description
This program allows the configuration of the ALTRO pedestal memory and of the
VFPED, BCTHR and ZSTRHR registers via the DDL. It expects files containing
either noise or pedestal information stored locally. The filename can be
given as an argument.

For compilation we used DATE v6.13.

Data source: TPC
Trigger type: Software or other
Run type: Calibration (Pedestal run)
Frequency of operation: End of Calibration (Pedestal) run
Non-RPM BuildRequires: Date
Non-RPM Requires: Date
Resources CPU: 
Resources disk: 
Resources memory: 
CFG files: none
FES files: none
Persistent files: none
TestData:
Executable: /opt/daq-TPC/TPCpedConfig

%prep
%setup -q

%build
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/opt/daq-TPC
install -s -m 755 TPCpedConfig $RPM_BUILD_ROOT/opt/daq-TPC/
install -s -m 755 TPC_PedestalConfiguration_Pattern.sh $RPM_BUILD_ROOT/opt/daq-TPC/
install -s -m 755 TPC_PedestalConfiguration_FPED.sh $RPM_BUILD_ROOT/opt/daq-TPC/
install -s -m 755 TPC_PedestalConfiguration_BCTHR.sh $RPM_BUILD_ROOT/opt/daq-TPC/
install -s -m 755 TPC_PedestalConfiguration_ZSTHR.sh $RPM_BUILD_ROOT/opt/daq-TPC/

%clean
make clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc

/opt/daq-TPC/TPCpedConfig
/opt/daq-TPC/TPC_PedestalConfiguration_BCTHR.sh
/opt/daq-TPC/TPC_PedestalConfiguration_Pattern.sh
/opt/daq-TPC/TPC_PedestalConfiguration_FPED.sh
/opt/daq-TPC/TPC_PedestalConfiguration_ZSTHR.sh

%changelog
* Wed Jun 11 2008  Christian.Lippmann@cern.ch - 0.8-1
- Bugfix concerning Eqid - RCU mapping
- Added debug options
- Compiled using DATE v6.13

* Wed Jun 11 2008  Christian.Lippmann@cern.ch - 0.7-1
- Ready for RCU Firmware Version 2
- To be tested at P2
- Compiled using DATE v6.13

* Tue Mar 25 2008  Christian.Lippmann@cern.ch - 0.6-1
- More bugfixes in WRITE_BCTHR and WRITE_ZSTHR
- Compiled using DATE v6.13

* Wed Jan 30 2008  Christian.Lippmann@cern.ch - 0.5-1
- Bugfixes in WRITE_BCTHR and WRITE_ZSTHR
- Compiled using DATE v6.13

* Wed Jan 02 2008  Christian.Lippmann@cern.ch - 0.4-1
- Introduced new function WRITE_TESTPATTERN
- Compiled using DATE v6.13

* Wed Nov 28 2007  Christian.Lippmann@cern.ch - 0.3-1
- FPED and PedMem configuration tested in December07 test run at P2.
- Introduced BCTHR, ZSTHR, not yet tested.
- Some Bug Fixes.
- Infologger not used explicitly anymore. Just printf.
- Compiled using DATE v6.13

* Wed Nov 21 2007  Christian.Lippmann@cern.ch - 0.2-1
- Added scripts to run on LDC.
- Compiled using DATE v6.13

* Tue Nov 20 2007  Christian.Lippmann@cern.ch - 0.1-1
- Initial build.
- Compiled using DATE v6.13

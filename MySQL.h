// -*- mode: C++ -*-
#ifndef TPCPEDCONFIG_MYSQL_H
#define TPCPEDCONFIG_MYSQL_H
#include <Db.h>

struct MySQL : public Db
{
  /** Type of result vector */
  typedef Db::Rows Rows;
  /** 
   * Constructor
   * 
   * @param url Connection URL
   */  
  MySQL(const std::string& url)
    : _is_open(false)
  {
    mysql_init(&_db);
    if (!mysql_real_connect(&_db, url.c_str())) { 
      std::cerr << "While connecting to " << url << ": " 
		<< mysql_error(_&db) << std::endl;
      return;
    }
    
    _is_open = true;
  }
  /** 
   * Destructor
   * 
   */
  ~MySQL() 
  {
    if (!_is_open) return;
    mysql_close(_db);
  }
  /** 
   * Perform query against Db
   * 
   * @param query Query 
   * @param rows  On return, contains the rows
   * 
   * @return @c true on success 
   */
  bool Query(const std::string& query, Rows& rows)
  {
    if (mysql_real_query(&_db, query.c_str(), query.size())) { 
      std::cerr << "While querying database: " 
		<< mysql_error(&_db) << std::endl;
      return false;
    }
    
    MYSQL_RES* res = mysql_store_result(&db);
    MYSQL_ROW  row;
    int n = mysql_num_fields(res);
    while ((row = mysql_fetch_row(res))) {
      rows.push_back(Row());
      Row& r = rows.back();
      for (int i = 0; i < n; i++) r._fields.push_back(row[i]);
    }
    
    mysql_free_result(res);
    return true;
  }
  /** 
   * Check if the connection is open
   * 
   * 
   * @return @c true if the connection is open.
   */
  bool IsOpen() const { return _is_open; }
protected:
  MYSQL _db;
  bool  _is_open;
};

#endif
//
// EOF
//

  

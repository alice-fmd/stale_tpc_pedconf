// -*- mode: C++ -*-
#ifndef TPCPEDCONFIG_RCU_H
#define TPCPEDCONFIG_RCU_H
#include <Db.h>
#include <string>
#include <vector>

/**
 * Abstract interface to RCU
 * 
 */
struct Rcu
{
  /** Vector of 32bit unsigned words */
  typedef std::vector<unsigned int> Words;
  /** 
   * Write a block
   * 
   * @param data Data to write
   * @param addr Address to write to
   * 
   * @return @c true on success, @c false otherwise
   */  
  virtual bool WriteBlock(const Words&  data, 
			  const unsigned short addr) = 0;
  /** 
   * Send a command to RCU
   * 
   * @param addr Address
   * 
   * @return @c true on success, @c false otherwise
   */
  virtual bool SendCommand(const unsigned short addr) = 0;
protected:
  /** 
   * Constructor 
   * 
   * @param eqid Equipment ID
   */  
  Rcu(int eqid=0) 
    : _id(0), 
      _eqid(eqid),
      _key(0),
      _ddl_revision(0),
      _ddl_serial(0),
      _ddl_channel(0),
      _afl(0),
      _hw_trigger(false),
      _status(0),
      _retry_count(0)
  {}
  /** 
   * Open the connection
   * 
   * @param rev   Revision
   * @param serno Serial number
   * @param chan  Channel
   * 
   * @return @c true on success
   */  
  bool DoOpen(int rev, int serno, int chan)
  {
    _key = get_channel(rev, serno, chan);
    if (_key->err) { 
      std::cerr << "Failed to open RORC (rev " << rev << ", serial " 
		<< serno << ", channel " << chan << ")"
		<< std::endl;
      return false;
    }
    _ddl_revision = rev;
    _ddl_serial   = serno;
    _ddl_channel  = channel;
    return true;
  }
  /** 
   * Close the connection
   * 
   * 
   * @return @c true
   */
  bool DoClose()
  {
    if (!_key || _key->err) return true;
    usleep(2000);
    release_channel(_key);
    return true;
  }
  /** 
   * Get details of this equipment from database
   * 
   * @param con Connection string
   * 
   * @return 
   */  
  bool GetDetails(Db& db)
  {
    Db::Rows rows;
    std::stringstream s;
    s << "SELECT rorcRevision,rorcSerialNb,rorcChannelNb "
      << "FROM EQUIP_PARAM_RorcData WHERE EqId=" << _eqid;
    if (!dn.Query(s.str(), rows)) return false;
    if (rows.size() < 0 || rows.size() > 1) {
      std::cerr << "Not enough/too many rows returned" << std::endl;
      return false;
    }
    Row& row = rows[0];
    if (row._fields.size() != 3) {
      std::cerr << "Not enough/too many fields returned" << std::endl;
      return false;
    }
    row.Value(0, _ddl_revision);
    row.Value(1, _ddl_serial);
    row.Value(2, _ddl_channel);
    return true;
  }
  /** Database ID */
  int _id;           
  /** Equipment ID */
  int _eqid;
  /** Returned channel key from Fec2Rorc library */
  chan_key* _key;
  /** DDL revision number */
  int _ddl_revision;
  /** DDL serial number */
  int _ddl_serial;
  /** DDL channel number */
  int _ddl_channel;
  /** active front-end-card list */
  unsigned int _afl
  /** HW/SW trigger setting */
  bool _hw_trigger;
  /** Status */
  int _status;
  /** Retry counter */
  int _retry_count;
};
#endif
//
// EOF
//

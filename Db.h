// -*- mode: C++ -*-
#ifndef TPCPEDCONFIG_DB
#define TPCPEDCONFIG_DB
#include <string>
#include <vector>
#include <sstream>

/**
 * Abstract interface to a database
 * 
 */
struct Db
{
  /**
   * Result of a query 
   * 
   */
  struct Row 
  {
    /** Type of field vector */
    typedef std::vector<std::string> Fields;
    /** Vector of fields */
    Fields _fields;
    /** 
     * Get a field value
     * 
     * @param i Field index
     * 
     * @return Field value, or the empty string
     */
    const std::string& operator[](size_t i) const 
    {
      static std::string empty;
      if (i >= _fields.size()) return empty;
      return _fields[i];
    }
    /** 
     * Get field value as any type
     * 
     * @param i Field number
     * @param v Value.
     */    
    template <typename T>
    void Value(size_t i , T& v) 
    {
      if (i >= _fields.size()) return;
      std::stringstream s(_fields[i]);
      s >> v;
    }
  };
  /** Type of a result vector */
  typedef std::vector<Row> Rows;
  /** 
   * Destructor 
   * 
   */
  virtual ~Db() {}
  /** 
   * Query the database
   * 
   * @param query Query to perform
   * @param r     Row vector
   * 
   * @return @c true on success
   */
  bool Query(const std::string& query, Rows& r) = 0;
  /** 
   * Check if the connection is open
   * 
   * 
   * @return @c true if the connection is opened.
   */  
  bool IsOpen() const { return false; }
protected:
  /** 
   * Constructor 
   * 
   */    
  Db() {}
};

#endif
//
// EOF
//

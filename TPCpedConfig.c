/*
 * TPCpedConfig.c
 * Run on LDC and write Pedestal data into Rcu memory
 * Author: C. Lippmann: Christian.Lippmann@cern.ch
 */

// standard includes
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>

// date/fec2rorc includes
#include <fec2rorc_lib.h>

// header file
#include "TPCpedConfig.h"

int i, j;
char filename[255];
char progname[255];
char hostname[255];
int factor, min_thr, max_thr, offset, occupancy;
rcu_model_t **rcu;

//========================================================================
int writeDummy_fw1(rcu_model_t* rcu, int nrcu) {
  //
  // Write ramp data to all pedestal memories for testing
  //

  int ret;

  // The pedestal buffer
  unsigned long pedestal_data[PEDCONF_RCU_PATTERN_MEM_SIZE];

  // Dummy Pedestal Data
  for ( i = 0; i < PEDCONF_RCU_PATTERN_MEM_SIZE; i++ )  pedestal_data[i] = i;

  // Write the pedestal data to the RCU_PATTERN_MEMORY
  ret = ddl_writeBlock(rcu->key, pedestal_data, 
		       PEDCONF_RCU_PATTERN_MEM_SIZE, 
		       PEDCONF_RCU_PATTERN_MEM_ADDR);
  if ( ret != 0 ) {
    if ( ret == -512 ) {
      printf("RCU Error. Returned bad data when writing pedestal on rcu %d\n", nrcu);
    } else {
      printf("Error writing pedestal ramp: rcu %d, error %i\n", nrcu, ret);
    }
    return ret;
  }
  usleep(2000);

  unsigned long ped_load[] ={ 0x381000,          // PMWRITE: Pedestal write instruction (broadcast)
			      PEDCONF_RCU_END }; // END of the instruction sequence

  // Write the Pedestal write instruction to the RCU_INSTRUCTION_MEMORY
  ret = ddl_writeBlock(rcu->key, ped_load, 2, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
  if ( ret != 0 ) {
    printf("Error writing ped_load instructions: %i\n", ret);
    return ret;
  }
  usleep(2000);

  // Execute the sequence in the instruction memory (the pedestal write instruction).
  ret = ddl_sendCommand(rcu->key, PEDCONF_RCU_EXEC);
  if ( ret != 0 ) {
    printf("Error in ddl_sendCommand: %i\n", ret);
    return ret;
  }

  return 0;
  
}

//========================================================================
int writeDummy_fw2(rcu_model_t* rcu, int nrcu) {
  //
  // Write ramp data to all pedestal memories for testing
  //

  int ret;

  unsigned long imem[PEDCONF_RCU_INSTRUCTION_MEM_SIZE];

  // Dummy Pedestal Data
  for ( i = 0; i < PEDCONF_RCU_INSTRUCTION_MEM_SIZE; i+=4 ) {
    imem[i+0] = 0x24000D;  // PMADD broadcast
    imem[i+1] = i/4;       // time bin 0 ... 1023
    imem[i+2] = 0x240007;  // PMDTA broadcast
    imem[i+3] = i/4;       // dummy value (ramp)
  }
  //imem[PEDCONF_RCU_INSTRUCTION_MEM_SIZE] = 380000; // no space for ENDSEQ; Will be executed anyway.

  /* Checking if the SIF Input Buffer is empty */
  if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
  ret = RCU_SIB_Empty_Wait(rcu);
  if (ret != 0) {
    if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
    return 1;
  }

  // Write the pedestal data to the RCU_INSTRUCTION_MEMORY
  ret = ddl_writeBlock(rcu->key, imem, PEDCONF_RCU_INSTRUCTION_MEM_SIZE, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
  if ( ret != 0 ) {
    if ( ret == -512 ) {
      printf("RCU Error. Returned bad data when writing pedestal on rcu %d\n", nrcu);
    } else {
      printf("Error writing pedestal ramp: rcu %d, error %i\n", nrcu, ret);
    }
    return ret;
  }

  /* Checking if the SIF Input Buffer is empty */
  if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
  ret = RCU_SIB_Empty_Wait(rcu);
  if (ret != 0) {
    if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
    return 1;
  }

  // Execute the sequence in the instruction memory
  ret = RCU_Exec_EXECSEQ(rcu);
  if ( ret != 0 ) {
    printf("Error in RCU_Exec_EXECSEQ(rcu): %i\n", ret);
    return ret;
  }

  // Waiting for the completion of the execution of the sequence
  usleep(10000);  /* temporary! */

  return 0;
}

//========================================================================
int writeTestPattern_fw1(rcu_model_t* rcu, int nrcu) {
  //
  // Write test pattern into RCU Pedestal Memory.
  //

  int ret;

  // The pedestal buffer
  unsigned long pedestal_data[PEDCONF_RCU_PATTERN_MEM_SIZE];
  for ( i = 0; i < PEDCONF_RCU_PATTERN_MEM_SIZE; i++ )  pedestal_data[i] = 0;

  // Dummy Pedestal Data
  filltestarray(pedestal_data, occupancy, 0);

  /*
  int ctr = 0;
  for ( i = 0; i < PEDCONF_RCU_PATTERN_MEM_SIZE; i++ ) {
    printf("%d: %d\n", i, (int)pedestal_data[i]);
    if (pedestal_data[i] > 45) ctr++;
  }
  printf("Ctr: %d\n", ctr);
  */

  if (debug) printf("*Writing pedestal data to RCU...\n");

  // Write the pedestal data to the RCU_PATTERN_MEMORY
  ret = ddl_writeBlock(rcu->key, pedestal_data, PEDCONF_RCU_PATTERN_MEM_SIZE, PEDCONF_RCU_PATTERN_MEM_ADDR);
  if ( ret != 0 ) {
    if ( ret == -512 ) {
      printf("RCU Error. Returned bad data when writing pedestal on rcu %d\n", nrcu);
    } else {
      printf("Error writing pedestal ramp: rcu %d, error %i\n", nrcu, ret);
    }
    return ret;
  }
  usleep(2000);

  unsigned long ped_load[] ={ 0x381000,     // PMWRITE: Pedestal write instruction (Broadcast Mode)
			      PEDCONF_RCU_END };   // END of the instruction sequence

  // Write the Pedestal write instruction to the RCU_INSTRUCTION_MEMORY
  ret = ddl_writeBlock(rcu->key, ped_load, 2, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
  if ( ret != 0 ) {
    printf("Error writing ped_load instructions: %i\n", ret);
    return ret;
  }
  usleep(2000);

  // Execute the sequence in the instruction memory (the pedestal write instruction).
  ret = ddl_sendCommand(rcu->key, PEDCONF_RCU_EXEC);

  if ( ret != 0 ) {
    printf("Error in ddl_sendCommand: %i\n", ret);
    return ret;
  }

  return 0;

}

//========================================================================
int writeTestPattern_fw2(rcu_model_t* rcu, int nrcu) {
  //
  // Write test pattern into RCU Pedestal Memory.
  //

  int ret;

  unsigned long pedestal_data[PEDCONF_ALTRO_PATTERN_MEM_SIZE];
  for ( i = 0; i < PEDCONF_ALTRO_PATTERN_MEM_SIZE; i++ ) pedestal_data[i] = 0;

  // Dummy Pedestal Data
  filltestarray(pedestal_data, occupancy, 15);

  /*
  int ctr = 0;
  for ( i = 0; i < 1000; i++ ) {
    printf("%d: %d\n", i, (int)pedestal_data[i]);
    if (pedestal_data[i] > 45) ctr++;
  }
  printf("Ctr: %d\n", ctr);
  */
  
  unsigned long imem[PEDCONF_RCU_INSTRUCTION_MEM_SIZE];
  for ( i = 0; i < PEDCONF_RCU_INSTRUCTION_MEM_SIZE; i++ ) imem[i] = 0;

  // Dummy Pedestal Data
  for ( i = 0; i < PEDCONF_RCU_INSTRUCTION_MEM_SIZE; i+=4 ) {
    imem[i+0] = 0x24000D;           // PMADD broadcast
    imem[i+1] = i/4;                // time bin 0 ... 1023
    imem[i+2] = 0x240007;           // PMDTA broadcast
    imem[i+3] = pedestal_data[i/4]; // value for this time bin
  }
  //imem[PEDCONF_RCU_INSTRUCTION_MEM_SIZE] = 380000; // no space for ENDSEQ; Will be executed anyway.

  /* Checking if the SIF Input Buffer is empty */
  if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
  ret = RCU_SIB_Empty_Wait(rcu);
  if (ret != 0) {
    if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
    return 1;
  }

  // Write the pedestal data to the RCU_INSTRUCTION_MEMORY
  ret = ddl_writeBlock(rcu->key, imem, PEDCONF_RCU_INSTRUCTION_MEM_SIZE, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
  if ( ret != 0 ) {
    if ( ret == -512 ) {
      printf("RCU Error. Returned bad data when writing pedestal on rcu %d\n", nrcu);
    } else {
      printf("Error writing pedestal ramp: rcu %d, error %i\n", nrcu, ret);
    }
    return ret;
  }

  /* Checking if the SIF Input Buffer is empty */
  if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
  ret = RCU_SIB_Empty_Wait(rcu);
  if (ret != 0) {
    if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
    return 1;
  }

  // Execute the sequence in the instruction memory
  ret = RCU_Exec_EXECSEQ(rcu);
  if ( ret != 0 ) {
    printf("Error in RCU_Exec_EXECSEQ(rcu): %i\n", ret);
    return ret;
  }

  // Waiting for the completion of the execution of the sequence
  usleep(10000);  /* temporary! */

  return 0;

}

//========================================================================
int writePattern_fw1(rcu_model_t* rcu, int nrcu) {
  //
  // Read pedestal files with time dependent data (one value per channel and time bin)
  // and write this to RCU Pedestal Mememory.
  //

  int ret;

  int tmp, side, sector, patch, hwaddr;
  int ok = 1;

  // open file with data
  FILE *input_file;
  if (debug) printf("*Opening file %s ...\n", filename);
  if ( (input_file = fopen(filename, "r")) == NULL ) {
    fprintf(stderr, "Cannot open file %s\n", filename);
    return 1;
  }

  // The pedestal buffer
  unsigned long pedestal_data[PEDCONF_RCU_PATTERN_MEM_SIZE];

  // read the first number in the file to check if it is pedmem file
  int test = readint(&ok, input_file);
  if ( test != 12 ) {
    printf("Error: Wrong file type (not a pedestal mem file)\n");
    return -1;
  }
  if (debug) printf("*Reading file and writing to RCU...\n");

  // Loop over all channels found in file
  while ( 1 ) {

    // read the first numbers from next line in the file
    i      = readint(&ok, input_file);
    side   = readint(&ok, input_file);
    sector = readint(&ok, input_file);
    patch  = readint(&ok, input_file);
    hwaddr = readint(&ok, input_file);

    if (!ok) break; // finished 

    if ( (side<0) || (side>1) ) {
      printf("Strange side number: %d\n", side);
      return -1;
    }

    if (  (sector<0) || (sector>17) ) {
      printf("Strange sector number: %d\n", sector);
      return -1;
    }

    if (  (patch<0) || (patch>5) ) {
      printf("Strange rcu number: %d\n", patch);
      return -1;
    }

    // Read Pedestal Data (skip 15 presamples)
    for ( j = 0; j < 15; j++ ) readfloat(&ok, input_file);  // do nothing with the presamples
    for ( j = 15; j < PEDCONF_RCU_PATTERN_MEM_SIZE; j++ ) {
      tmp = (rounding) ? roundtoint(readfloat(&ok, input_file)) : (unsigned long)readfloat(&ok, input_file);
      pedestal_data[j-15] = (tmp > PEDCONF_ALTRO_PATTERN_MEM_SIZE) ? 0 : (tmp); // funny value?
    }
    for ( j = PEDCONF_RCU_PATTERN_MEM_SIZE-15; j < PEDCONF_RCU_PATTERN_MEM_SIZE; j++ ) pedestal_data[j] = 0;
    
    if ( patch == nrcu ) {  // only configure this rcu

      if (debug) 
	printf("*Writing pattern: Channel %d, altro %d, fec %d, branch %d (hwaddr %d), rcu %d\n",
	       decodedAddressChanneladdr(hwaddr), decodedAddressChipaddr(hwaddr),
	       decodedAddressFECaddr(hwaddr), decodedAddressBranch(hwaddr), hwaddr, nrcu);
    
      // Write the pedestal data to the RCU_PATTERN_MEMORY
      ret = 0;
      do {
	ret = ddl_writeBlock(rcu->key, pedestal_data, PEDCONF_RCU_PATTERN_MEM_SIZE, PEDCONF_RCU_PATTERN_MEM_ADDR);
	if ( ret != 0 ) {
	  if ( ret == -512 ) {
	    printf("RCU Error. Returned bad data when writing pedestal on rcu %d, hwaddr %d. Try again ...\n",
		   nrcu, hwaddr);
	  } else {
	    printf("Error writing pedestal ramp: rcu %d, error %i. Try again ...\n", nrcu, ret);
	  }
	}
	usleep(2000);
      } while (ret == -512); // try again

      unsigned long ped_load[] ={ 0x380000 | hwaddr,   // PMWRITE: Pedestal write instruction
				  PEDCONF_RCU_END };   // END of the instruction sequence
      
      // Write the Pedestal write instruction to the RCU_INSTRUCTION_MEMORY
      ret = ddl_writeBlock(rcu->key, ped_load, 2, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
      if ( ret != 0 ) {
	printf("Error writing ped_load instructions: %i\n", ret);
	return ret;
      }
      usleep(2000);
      
      // Execute the sequence in the instruction memory (the pedestal write instruction).
      ret = ddl_sendCommand(rcu->key, PEDCONF_RCU_EXEC);
      if ( ret != 0 ) {
	printf("Error in ddl_sendCommand: %i\n", ret);
	return ret;
      }
      usleep(2000);

    }  // end if (patch == nrcu)
    
  } // end while (file loop)
  
  // close the file
  fclose(input_file);

  return 0;
}

//========================================================================
int writePattern_fw2(rcu_model_t* rcu, int nrcu) {
  //
  // Read pedestal files with time dependent data (one value per channel and time bin)
  // and write this to RCU Pedestal Mememory.
  //

  int tmp, side, sector, patch, hwaddr, chipaddr;
  int ok = 1;

  int ret;

  // open file with data
  FILE *input_file;
  if (debug) printf("*Opening file %s ...\n", filename);
  if ( (input_file = fopen(filename, "r")) == NULL ) {
    fprintf(stderr, "Cannot open file %s\n", filename);
    return 1;
  }

  // The pedestal buffer
  unsigned long pedestal_data[PEDCONF_ALTRO_PATTERN_MEM_SIZE];
  for ( i = 0; i < PEDCONF_ALTRO_PATTERN_MEM_SIZE; i++ ) pedestal_data[i] = 0;

  unsigned long imem[PEDCONF_RCU_INSTRUCTION_MEM_SIZE];
  for ( i = 0; i < PEDCONF_RCU_INSTRUCTION_MEM_SIZE; i++ ) imem[i] = 0;

  // read the first number in the file to check if it is pedmem file
  int test = readint(&ok, input_file);
  if ( test != 12 ) {
    printf("Error: Wrong file type (not a pedestal mem file)\n");
    return -1;
  }
  if (debug) printf("*Reading file and writing to RCU...\n");

  // Loop over all channels found in file
  while ( 1 ) {

    // read the first numbers from next line in the file
    i      = readint(&ok, input_file);
    side   = readint(&ok, input_file);
    sector = readint(&ok, input_file);
    patch  = readint(&ok, input_file);
    hwaddr = readint(&ok, input_file);

    if (!ok) break; // finished 

    if ( (side<0) || (side>1) ) {
      printf("Strange side number: %d\n", side);
      return -1;
    }
    if (  (sector<0) || (sector>17) ) {
      printf("Strange sector number: %d\n", sector);
      return -1;
    }
    if (  (patch<0) || (patch>5) ) {
      printf("Strange rcu number: %d\n", patch);
      return -1;
    }

    chipaddr =  (hwaddr>>4) & 0xff; // branch, FEC and ALTRO bits

    // Read Pedestal Data
    for ( j = 0; j < PEDCONF_ALTRO_PATTERN_MEM_SIZE; j++ ) {
      tmp = (rounding) ? roundtoint(readfloat(&ok, input_file)) : (unsigned long)readfloat(&ok, input_file);
      pedestal_data[j] = (tmp > PEDCONF_ALTRO_PATTERN_MEM_SIZE) ? 0 : (tmp); // funny value?
    }

    if ( patch == nrcu ) {  // only configure this rcu

      if (debug) 
	printf("*Writing pattern: Channel %d, altro %d, fec %d, branch %d (hwaddr %d), rcu %d\n",
	       decodedAddressChanneladdr(hwaddr), decodedAddressChipaddr(hwaddr),
	       decodedAddressFECaddr(hwaddr), decodedAddressBranch(hwaddr), hwaddr, nrcu);

      // Create IMEM block
      for ( i = 0; i < PEDCONF_RCU_INSTRUCTION_MEM_SIZE; i += 4 ) {
	imem[i+0] = (unsigned long)((2<<20) | (chipaddr<<9) | 0xD); // PMADD
	imem[i+1] = (unsigned long)(i/4);                           // time bin 0 ... 1023
	imem[i+2] = (unsigned long)((2<<20) | (hwaddr<<5)   | 0x7); // PMDTA
	imem[i+3] = (unsigned long)pedestal_data[i/4];              // value for this time bin
	/*
	printf("0x%x\n",(unsigned int)imem[i+0]);
	printf("0x%x\n",(unsigned int)imem[i+1]);
	printf("0x%x\n",(unsigned int)imem[i+2]);
	printf("0x%x\n",(unsigned int)imem[i+3]);
	*/
      }
      //imem[4096] = 380000; // no space for ENDSEQ; Will be executed anyway.

      /* Checking if the SIF Input Buffer is empty */
      if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
      ret = RCU_SIB_Empty_Wait(rcu);
      if (ret != 0) {
	if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
	return 1;
      }

      // Write the pedestal data to the RCU_INSTRUCTION_MEMORY
      ret = ddl_writeBlock(rcu->key, imem, PEDCONF_RCU_INSTRUCTION_MEM_SIZE, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
      if ( ret != 0 ) {
	if ( ret == -512 ) {
	  printf("RCU Error. Returned bad data when writing pedestal on rcu %d\n", nrcu);
	} else {
	  printf("Error writing pedestal ramp: rcu %d, error %i\n", nrcu, ret);
	}
	return ret;
      }

      /* Checking if the SIF Input Buffer is empty */
      if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
      ret = RCU_SIB_Empty_Wait(rcu);
      if (ret != 0) {
        if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
        return 1;
      }

      // Execute the sequence in the instruction memory
      ret = RCU_Exec_EXECSEQ(rcu);
      if ( ret != 0 ) {
	printf("Error in RCU_Exec_EXECSEQ(rcu): %i\n", ret);
	return ret;
      }

      // Waiting for the completion of the execution of the sequence
      usleep(10000);  /* temporary! */

   }  // end if (patch == nrcu)
    
  } // end while (file loop)
  
  // close the file
  fclose(input_file);

  return 0;

}

//========================================================================
int writeFPED(rcu_model_t* rcu, int nrcu) {
  //
  // Write the fixed pedestal values from file to ALTROs
  //

  int ret;

  unsigned long pedValue;
  int side, sector, patch, hwaddr;
  int ok = 1;

  // open file with data
  FILE *input_file;
  if (debug) printf("*Opening file %s ...\n", filename);
  if ( (input_file = fopen(filename, "r")) == NULL ) {
    fprintf(stderr, "Cannot open file %s\n", filename);
    return 1;
  }

  u_long imem[PEDCONF_RCU_INSTRUCTION_MEM_SIZE];
  int memctr = 0;

  // read the first number in the file to check if it is pedestal file
  int test = readint(&ok, input_file);
  if ( test != 10 ) {
    printf("Error: Wrong file type (not a pedestal file)\n");
    return -1;
  }

  if (debug) printf("*Reading file and writing to RCU...\n");

  // Loop over all channels found in file
  while ( 1 ) {

    // read numbers from one line in the file
    i      = readint(&ok, input_file);
    side   = readint(&ok, input_file);
    sector = readint(&ok, input_file);
    patch  = readint(&ok, input_file);
    hwaddr = readint(&ok, input_file);
    pedValue = (rounding) ? roundtoint(readfloat(&ok, input_file)) : (unsigned long)readfloat(&ok, input_file);

    if ( ok && ((side<0) || (side>1)) ) {
      printf("Strange side number: %d\n", side);
      return -1;
    }

    if ( ok && ((sector<0) || (sector>17)) ) {
      printf("Strange sector number: %d\n", sector);
      return -1;
    }

    if ( ok && ((patch<0) || (patch>5)) ) {
      printf("Strange rcu number: %d\n", patch);
      return -1;
    }

    if (pedValue == 0) continue;

    if ( ok && (nrcu == patch) ) {  // only configure the rcu connected to this rorc!
      if (debug) 
	printf("*Writing FPED=%d: Channel %d, altro %d, fec %d, branch %d (hardware address %d)\n",
	       (int)pedValue, decodedAddressChanneladdr(hwaddr), decodedAddressChipaddr(hwaddr),
	       decodedAddressFECaddr(hwaddr), decodedAddressBranch(hwaddr), hwaddr);
      
      if (PEDCONF_RCU_FW_VERSION == 1) {
	imem[memctr++] = ((0x600000 | (hwaddr<<5)) | 0x6);  // 0x6 -> FPED register adress
	imem[memctr++] =  (0x700000 | (pedValue&0x3ff));    // 10bit wide fixed pedestal value
      } else if (PEDCONF_RCU_FW_VERSION == 2) {
	imem[memctr++] = ((0x200000 | (hwaddr<<5)) | 0x6);  // 0x6 -> FPED register adress
	imem[memctr++] =  pedValue&0x3ff;                   // 10bit wide fixed pedestal value
      }
      /*
      printf("rcu %d, 0x%x\n",nrcu, (unsigned int)imem[memctr-2]);
      printf("rcu %d, 0x%x\n",nrcu, (unsigned int)imem[memctr-1]);
      */
    }

    if ( debug && (memctr >= (PEDCONF_RCU_INSTRUCTION_MEM_SIZE - 4)) ) printf("*IMEM full.\n");
    if ( debug && !ok )                                                printf("*End of file.\n");

    if ( (memctr >= (PEDCONF_RCU_INSTRUCTION_MEM_SIZE - 4)) || !ok ) {  // IMEM almost full or finished

      imem[memctr++] = PEDCONF_RCU_END;  // RCU_END instruction

      //for (j=0; j<memctr; j++) printf("rcu%d, 0x%x\n", nrcu, imem[j]);

      /* Checking if the SIF Input Buffer is empty */
      if (PEDCONF_RCU_FW_VERSION == 2) {
	if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
	ret = RCU_SIB_Empty_Wait(rcu);
	if (ret != 0) {
	  if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
	  return 1;
	}
      }

      // write to IMEM
      if (debug) printf("*Writing to IMEM\n");
      ret = ddl_writeBlock(rcu->key, imem, memctr, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
      if (debug) printf("*Finished writing to IMEM\n");
      if(ret != 0) {
	printf("Error writing to instruction memory: %i\n", ret);
	return ret;
      }

      /* Checking if the SIF Input Buffer is empty */
      if (PEDCONF_RCU_FW_VERSION == 2) {
	if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
	ret = RCU_SIB_Empty_Wait(rcu);
	if (ret != 0) {
	  if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
	  return 1;
	}
      }

      // Execute the sequence in the imem.
      if (PEDCONF_RCU_FW_VERSION == 1) {
	ret = ddl_sendCommand(rcu->key, PEDCONF_RCU_EXEC);
	if ( ret != 0 ) {
	  printf("Error in ddl_sendCommand: %i\n", ret);
	  return ret;
	}
      } else if (PEDCONF_RCU_FW_VERSION == 2) {
	ret = RCU_Exec_EXECSEQ(rcu);
	if ( ret != 0 ) {
	  printf("Error in RCU_Exec_EXECSEQ(rcu): %i\n", ret);
	  return ret;
	}
      }
      usleep(1000);   /* temporary! */
      memctr = 0;
	
    } // end if

    if ( !ok ) break;

  } // end while (file loop)

  // close the file
  fclose(input_file);

  return 0;
}


//========================================================================
int writeBCTHR(rcu_model_t* rcu, int nrcu) {
  //
  // Write the thresholds for the moving average filter (second baseline restoration filter)
  //

  int ret;

  unsigned long THR_HI, THR_LO;
  int side, sector, patch, hwaddr;
  int ok = 1;
  
  // open file with data
  FILE *input_file;
  if (debug) printf("*Opening file %s ...\n", filename);
  if ( (input_file = fopen(filename, "r")) == NULL ) {
    fprintf(stderr, "Cannot open file %s\n", filename);
    return 1;
  }

  u_long imem[PEDCONF_RCU_INSTRUCTION_MEM_SIZE];
  int memctr = 0;

  // read the first number in the file and check if it is noise file
  int test = readint(&ok, input_file);
  if ( test != 11 ) {
    printf("Error: Wrong file type (not a noise file)\n");
    return -1;
  }

  if (debug) printf("*Reading file and writing to RCU...\n");

  // Loop over all channels found in file
  while ( 1 ) {

    // read the three numbers from one line in the file
    i      = readint(&ok, input_file);
    side   = readint(&ok, input_file);
    sector = readint(&ok, input_file);
    patch  = readint(&ok, input_file);
    hwaddr = readint(&ok, input_file);

    if ( ok && ((side<0) || (side>1)) ) {
      printf("Strange side number: %d\n", side);
      return -1;
    }

    if ( ok && ((sector<0) || (sector>17)) ) {
      printf("Strange sector number: %d\n", sector);
      return -1;
    }

    if ( ok && ((patch<0) || (patch>5)) ) {
      printf("Strange rcu number: %d\n", patch);
      return -1;
    }

    // Read the noise value
    float tmp = readfloat(&ok, input_file) * factor;
    THR_LO = (rounding) ? roundtoint(tmp) : (unsigned long)tmp;
    if ( THR_LO > max_thr ) THR_LO = max_thr;
    if ( THR_LO < min_thr ) THR_LO = min_thr;
    THR_HI = THR_LO;   // at the moment we have the same thresholds hi and lo

    if ( ok && (nrcu == patch) ) {  // only configure the rcu connected to this rorc!

      if (debug)
	printf("*Writing BCTHR (Lo=%d, Hi=%d): Rcu %d, branch %d, fec %d, altro %d, channel %d (hardware address %d)\n",
	       (int)(THR_LO&0x3ff), (int)(THR_HI&0x3ff), nrcu, decodedAddressBranch(hwaddr),
	       decodedAddressFECaddr(hwaddr), decodedAddressChipaddr(hwaddr),
	       decodedAddressChanneladdr(hwaddr), hwaddr);
	
      if (PEDCONF_RCU_FW_VERSION == 1) {
	imem[memctr++] = ((0x600000 | (hwaddr << 5) ) | 0x9);                    // 0x9 -> BCTHR register adress
	imem[memctr++] =   0x700000 | ( ((THR_HI&0x3ff)<<10) | (THR_LO&0x3ff) ); // Two 10bit wide values
      } else if (PEDCONF_RCU_FW_VERSION == 2) {
	imem[memctr++] = ((0x200000 | (hwaddr << 5) ) | 0x9);        	         // 0x9 -> BCTHR register adress
	imem[memctr++] = ((THR_HI&0x3ff)<<10) | (THR_LO&0x3ff);                  // Two 10bit wide values
      }
    }

    if ( debug && (memctr >= (PEDCONF_RCU_INSTRUCTION_MEM_SIZE - 4)) ) printf("*IMEM full.\n");
    if ( debug && !ok )                                                printf("*End of file.\n");

    if ( (memctr >= (PEDCONF_RCU_INSTRUCTION_MEM_SIZE - 4)) || !ok ) {  // IMEM almost full or finished
      
      imem[memctr++] = PEDCONF_RCU_END;  // RCU_END instruction

      /* Checking if the SIF Input Buffer is empty */
      if (PEDCONF_RCU_FW_VERSION == 2) {
	if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
	ret = RCU_SIB_Empty_Wait(rcu);
	if (ret != 0) {
	  if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
	  return 1;
	}
      }

      // write to IMEM
      ret = ddl_writeBlock(rcu->key, imem, memctr, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
      if(ret != 0) {
	printf("Error writing to instruction memory: %i\n", ret);
	return ret;
      }
    
      /* Checking if the SIF Input Buffer is empty */
      if (PEDCONF_RCU_FW_VERSION == 2) {
	if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
	ret = RCU_SIB_Empty_Wait(rcu);
	if (ret != 0) {
	  if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
	  return 1;
	}
      }

      // Execute the sequence in the imem.
      if (PEDCONF_RCU_FW_VERSION == 1) {
	ret = ddl_sendCommand(rcu->key, PEDCONF_RCU_EXEC);
	if ( ret != 0 ) {
	  printf("Error in ddl_sendCommand: %i\n", ret);
	  return ret;
	}
      } else if (PEDCONF_RCU_FW_VERSION == 2) {
	ret = RCU_Exec_EXECSEQ(rcu);
	if ( ret != 0 ) {
	  printf("Error in RCU_Exec_EXECSEQ(rcu): %i\n", ret);
	  return ret;
	}
      }
      usleep(1000);   /* temporary! */
      memctr = 0;

    }

    if ( !ok ) break;

  } // end while (file loop)

  // close the file
  fclose(input_file);

  return 0;

}


//========================================================================
int writeZSTHR(rcu_model_t* rcu, int nrcu) {
  //
  // Write the thresholds for the zero suppression filter
  //

  int ret;

  unsigned long OFFSET, ZS_THR;
  int side, sector, patch, hwaddr;
  int ok = 1;

  OFFSET = offset;

  // open file with data
  FILE *input_file;
  if (debug) printf("*Opening file %s ...\n", filename);
  if ( (input_file = fopen(filename, "r")) == NULL ) {
    fprintf(stderr, "Cannot open file %s\n", filename);
    return 1;
  }

  u_long imem[PEDCONF_RCU_INSTRUCTION_MEM_SIZE];
  int memctr = 0;

  // read the first number in the file and check if it is noise file
  int test = readint(&ok, input_file);
  if ( test != 11 ) {
    printf("Error: Wrong file type (not a noise file)\n");
    return -1;
  }

  if (debug) printf("*Reading file and writing to RCU...\n");

  // Loop over all channels found in file
  while ( 1 ) {

    // read the three numbers from one line in the file
    i      = readint(&ok, input_file);
    side   = readint(&ok, input_file);
    sector = readint(&ok, input_file);
    patch  = readint(&ok, input_file);
    hwaddr = readint(&ok, input_file);

    if ( ok && ((side<0) || (side>1)) ) {
      printf("Strange side number: %d\n", side);
      return -1;
    }

    if ( ok && ((sector<0) || (sector>17)) ) {
      printf("Strange sector number: %d\n", sector);
      return -1;
    }

    if ( ok && ((patch<0) || (patch>5)) ) {
      printf("Strange rcu number: %d\n", patch);
      return -1;
    }

    // Read the noise value
    float tmp = readfloat(&ok, input_file) * factor;
    ZS_THR = (rounding) ? roundtoint(tmp) : (unsigned long)tmp;
    if ( ZS_THR > max_thr ) ZS_THR = max_thr;
    if ( ZS_THR < min_thr ) ZS_THR = min_thr;

    if ( ok && (nrcu == patch) ) {  // only configure the rcu connected to this rorc!
      if (debug)
	printf("*Writing ZSTHR=%d: Rcu %d, branch %d, fec %d, altro %d, channel %d (hardware address %d)\n",
	       (int)(ZS_THR&0x3ff), nrcu, decodedAddressBranch(hwaddr), decodedAddressFECaddr(hwaddr),
	       decodedAddressChipaddr(hwaddr), decodedAddressChanneladdr(hwaddr), 
	       hwaddr);
	
      if (PEDCONF_RCU_FW_VERSION == 1) {
	imem[memctr++] = ((0x600000 | (hwaddr<<5)) | 0x8);                     // 0x8 -> ZSTHR register adress
	imem[memctr++] =   0x700000 | (((OFFSET&0x3ff)<<10) | (ZS_THR&0x3ff)); // Two 10bit wide values
      } else if (PEDCONF_RCU_FW_VERSION == 2) {
	imem[memctr++] = ((0x200000 | (hwaddr<<5)) | 0x8);                     // 0x8 -> ZSTHR register adress
	imem[memctr++] = ((OFFSET&0x3ff)<<10) | (ZS_THR&0x3ff);                // Two 10bit wide values
      }
    }

    if ( debug && (memctr >= (PEDCONF_RCU_INSTRUCTION_MEM_SIZE - 4)) ) printf("*IMEM full.\n");
    if ( debug && !ok )                                                printf("*End of file.\n");

    if ( (memctr >= (PEDCONF_RCU_INSTRUCTION_MEM_SIZE - 4)) || !ok ) { // IMEM almost full or finished

      imem[memctr++] = PEDCONF_RCU_END;  // RCU_END instruction

      /* Checking if the SIF Input Buffer is empty */
      if (PEDCONF_RCU_FW_VERSION == 2) {
	if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
	ret = RCU_SIB_Empty_Wait(rcu);
	if (ret != 0) {
	  if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
	  return 1;
	}
      }

      // write to IMEM
      ret = ddl_writeBlock(rcu->key, imem, memctr, PEDCONF_RCU_INSTRUCTION_MEM_ADDR);
      if ( ret != 0 ) {
	printf("Error writing to instruction memory: %i\n", ret);
	return ret;
      }

      /* Checking if the SIF Input Buffer is empty */
      if (PEDCONF_RCU_FW_VERSION == 2) {
	if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
	ret = RCU_SIB_Empty_Wait(rcu);
	if (ret != 0) {
	  if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
	  return 1;
	}
      }

      // Execute the sequence in the imem.
      if (PEDCONF_RCU_FW_VERSION == 1) {
	ret = ddl_sendCommand(rcu->key, PEDCONF_RCU_EXEC);
	if ( ret != 0 ) {
	  printf("Error in ddl_sendCommand: %i\n", ret);
	  return ret;
	}
      } else if (PEDCONF_RCU_FW_VERSION == 2) {
	ret = RCU_Exec_EXECSEQ(rcu);
	if ( ret != 0 ) {
	  printf("Error in RCU_Exec_EXECSEQ(rcu): %i\n", ret);
	  return ret;
	}
      }
      usleep(1000);   /* temporary! */
      memctr = 0;

    }

    if ( !ok ) break;

  } // end while  (file loop)

  // close the file
  fclose(input_file);

  return 0;
}


//========================================================================
//========================================================================
//========================================================================
int main( int argc, char** argv ) {
  //
  // Configure the RCU
  //

  int ret = 0;

  sprintf(progname, "%s", "TPCCpedConfig");

  /* get hostname of this LDC */
  if (gethostname (hostname, 255) != 0) {
    printf("ERROR: Routine gethostname is failing");
    return -1;
  }

  // Rorc information for RCU lab setup
  int rorc_revision = 3;
  int rorc_serial   = 517;
  int rorc_channel  = 0;

  int status;

  int command = 0;
  int nrcu = -1;

  bool on_ldc = true;

  PEDCONF_RCU_FW_VERSION = 2; // RCU Firmware Version 2 is default now

  // Some default values
  factor  = 3;
  max_thr = 10;
  min_thr = 2;
  offset  = 0;

  // Check arguments
  if (argc < 2) {
    printhelp();
    return 1;
  }

  // Check arguments
  for ( i = 1; i < argc; i++ ) {
    if ( (strcmp( argv[i], "-h" ) == 0) || (strcmp( argv[i], "-help" ) == 0) ) {
      printhelp();
      return 1;
    } else {
      if ( strcmp(argv[i], "-fwv") == 0 ) {
        if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) {
	  PEDCONF_RCU_FW_VERSION = atoi( argv[i+1] );
	  if ( (PEDCONF_RCU_FW_VERSION < 0) || (PEDCONF_RCU_FW_VERSION > 2) ) {
	    printf("Wrong RCU Firmware version!\n");
	    return 1;
	  }
	}
      } else if ( strcmp(argv[i], "-debug") == 0 ) {
	debug = true;
      } else if ( strcmp(argv[i], "-revision") == 0 ) {
        if ( (i+1) < argc ) {
	  if ( (strcmp(argv[i+1], "-1") == 0 ) || (argv[i+1][0] != '-') ) {
	    rorc_revision = atoi( argv[i+1] );
	    on_ldc = false;
	  }
	}
      } else if ( strcmp(argv[i], "-serial") == 0 ) {
        if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) {
	  rorc_serial = atoi( argv[i+1] );
	  on_ldc = false;
	}
      } else if ( strcmp(argv[i], "-channel") == 0 ) {
        if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) {
	  rorc_channel = atoi( argv[i+1] );
	  on_ldc = false;
	}
      } else if ( strcmp(argv[i], "-minor") == 0 ) {
        if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) {
	  rorc_revision = -1;
	  rorc_serial   = atoi( argv[i+1] );
	  on_ldc = false;
	}
      } else if ( strcmp(argv[i], "-rcu") == 0 ) {
        if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) {
	  nrcu = atoi( argv[i+1] );
	  on_ldc = false;
	}
      } else if ( strcmp(argv[i], "-factor") == 0 ) {
	if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) factor = atoi( argv[i+1] );
      } else if ( strcmp(argv[i], "-max") == 0 ) {
	if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) max_thr = atoi( argv[i+1] );
      } else if ( strcmp(argv[i], "-min") == 0 ) {
	if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) min_thr = atoi( argv[i+1] );
      } else if ( strcmp(argv[i], "-offset") == 0 ) {
	if ( ((i+1) < argc) && (argv[i+1][0] != '-') ) offset = atoi( argv[i+1] );
      } else if ( strcmp(argv[i], "-ramp") == 0 ) {
        command = PEDCONF_WRITE_RAMP;
      } else if ( strcmp(argv[i], "-testpattern") == 0 ) {
	if ( (((i+1) < argc) && (argv[i+1][0] == '-')) || ((i+1) == argc) ) {
	  printf("Specify occupancy!\n");
	  return 1;
	}
	occupancy = atoi( argv[i+1] );
	if (occupancy <   0) occupancy = 0;
	if (occupancy > 100) occupancy = 100;
        command = PEDCONF_WRITE_TESTPATTERN;
      } else if ( strcmp(argv[i], "-pattern") == 0 ) {
	if ( (((i+1) < argc) && (argv[i+1][0] == '-')) || ((i+1) == argc) ) {
	  printf("Give a filename!\n");
	  return 1;
	}
	sprintf(filename, argv[i+1]);
        command = PEDCONF_WRITE_PATTERN;
      } else if ( strcmp(argv[i], "-fped") == 0 ) {
	if ( (((i+1) < argc) && (argv[i+1][0] == '-')) || ((i+1) == argc) ) {
	  printf("Give a filename!\n");
	  return 1;
	}
	sprintf(filename, argv[i+1]);
        command = PEDCONF_WRITE_FPED;
      } else if ( strcmp(argv[i], "-bcthr") == 0 ) {
	if ( (((i+1) < argc) && (argv[i+1][0] == '-')) || ((i+1) == argc) ) {
	  printf("Give a filename!\n");
	  return 1;
	}
	sprintf(filename, argv[i+1]);
        command = PEDCONF_WRITE_BCTHR;
      } else if ( strcmp(argv[i], "-zsthr") == 0 ) {
	if ( (((i+1) < argc) && (argv[i+1][0] == '-')) || ((i+1) == argc) ) {
	  printf("Give a filename!\n");
	  return 1;
	}
	sprintf(filename, argv[i+1]);
        command = PEDCONF_WRITE_ZSTHR;
      }
    }
  }

  /* Which RCU Firmware Version do we use? */
  if (PEDCONF_RCU_FW_VERSION == 1) {
    PEDCONF_RCU_INSTRUCTION_MEM_ADDR = 0x7000;
    PEDCONF_RCU_PATTERN_MEM_ADDR     = 0x6800; 
    PEDCONF_RCU_INSTRUCTION_MEM_SIZE =  256;
    PEDCONF_RCU_PATTERN_MEM_SIZE     = 1000; 
    PEDCONF_RCU_END        = 0x390000;
    PEDCONF_RCU_EXEC       = 0x0;
  } else if (PEDCONF_RCU_FW_VERSION == 2) {
    PEDCONF_RCU_INSTRUCTION_MEM_ADDR = 0x0;
    PEDCONF_RCU_INSTRUCTION_MEM_SIZE = 4096;
    PEDCONF_RCU_END        = 0x380000;
    PEDCONF_RCU_EXEC       = 0x5304;
  }

  /* prepare pointers to the RCU models */
  rcu = calloc(6, sizeof(*rcu));
  if (rcu == NULL) {
    printf("ERROR: Allocation of rcu in failed for %s", hostname);
    return -1;
  }
  for (i = 0; i < 6; i++) rcu[i] = NULL;

  int eqid_array[6] = {-1,-1,-1,-1,-1,-1};   // The equipment IDs
  int rcu_array[6]  = {-1,-1,-1,-1,-1,-1};   // The RCU numbers
  pid_t eqid_pid[6] = {-1,-1,-1,-1,-1,-1};   // The process ids

  int num_eqid = -1;

  if ( !on_ldc ) {  // rorc details are provided by user

    num_eqid = 6;

    if ( nrcu == -1 ) {
      printf("RCU (0...5) needs to be specified. Specify with \"-rcu <number>\" !\n");
      return 1;
    }
    if ( (nrcu < 0) || (nrcu > 5) ) {
      printf("RCU has to be in range (0...5)!\n");
      return 1;
    }

    /* allocate a new RCU model */
    rcu[nrcu] = get_new_rcu_model();

    /* set values in the RCU model */
    rcu[nrcu]->eqid         = 0;
    rcu[nrcu]->ddl_revision = rorc_revision;
    rcu[nrcu]->ddl_serial   = rorc_serial;
    rcu[nrcu]->ddl_channel  = rorc_channel;

    eqid_array[nrcu] = rcu[nrcu]->eqid;
    rcu_array[nrcu]  = nrcu;

  } else {    // rorc details are provided by mysql

    num_eqid = get_eqid(eqid_array, 6);
    if (num_eqid <= 0) {
      printf("ERROR: No configured RorcData equipment for %s\n", hostname);
      return -1;
    }
    if (num_eqid > 6) {
      printf("ERROR: Too many configured RorcData equipments for %s, configured=%d, maximum=6\n",
	     hostname, num_eqid);
      return -1;
    }

    for (i = 0; i < num_eqid; i++) {      /* for each rcu */

      rcu_array[i] = getPatchFromEquipmentID(eqid_array[i]);

      /* allocated a new RCU model */
      rcu[i] = get_new_rcu_model();

      /* set the eqId in the RCU model */
      rcu[i]->eqid = eqid_array[i]; 

      /* load from the DATE database the active RORCs for this eqId into the RCU model */
      ret = get_ddl_details(rcu[i]);
      if (ret != 0) {
	printf("ERROR: No configured RORC for eqid %i at %s, ret=%d\n",
	       rcu[i]->eqid,  hostname, ret);
	continue;
      }

    } // end loop for getting rcu details

  } // end if on_ldc

  for (i = 0; i < num_eqid; i++) {  // RORC/RCU loop

    if ( eqid_array[i] == -1 ) {  // No RCU found
      //printf("Warning: Not properly configured eqid at %s\n", hostname);
      continue;
    }

    // Open the RORC
    rcu[i]->key = get_channel(rcu[i]->ddl_revision, rcu[i]->ddl_serial, rcu[i]->ddl_channel);

    if (rcu[i]->key->err != 0) {
      printf ("Failed to open RORC (rev %d, s/n %d, channel %d) at %s "
	      "with eqid %i", rcu[i]->ddl_revision, rcu[i]->ddl_serial, rcu[i]->ddl_channel,
	      hostname, rcu[i]->eqid); 
      continue;
    }

    if (debug) {
      printf ("EqId %3d: RORC rev %d, s/n %d, channel %d \n", rcu[i]->eqid, rcu[i]->ddl_revision,
	      rcu[i]->ddl_serial, rcu[i]->ddl_channel);
    }

    /*** FORK ***/
    pid_t pid = fork();
    if ( pid < 0 ) {
      printf ("ERROR: fork failed \n");
      return (-1);
    }

    /*** child process starts here ***/
    if (pid == 0) { 

      /* execute the commands for one RCU */
      switch ( command ) {
      case PEDCONF_WRITE_RAMP:
	// dummy pedestal memory configuration (ramp)
	printf ("EqId %03d (rcu %d): WRITE_RAMP command is being executed ...\n",
		rcu[i]->eqid, rcu_array[i]);
	if ( PEDCONF_RCU_FW_VERSION == 1 ) ret = writeDummy_fw1(rcu[i], rcu_array[i]);
	if ( PEDCONF_RCU_FW_VERSION == 2 ) ret = writeDummy_fw2(rcu[i], rcu_array[i]);
	break;
      case PEDCONF_WRITE_TESTPATTERN:
	printf ("EqId %03d (rcu %d): WRITE_TESTPATTERN command is being executed ...\n",
		rcu[i]->eqid, rcu_array[i]);
	if ( PEDCONF_RCU_FW_VERSION == 1 ) ret = writeTestPattern_fw1(rcu[i], rcu_array[i]);
	if ( PEDCONF_RCU_FW_VERSION == 2 ) ret = writeTestPattern_fw2(rcu[i], rcu_array[i]);
	break;
      case PEDCONF_WRITE_PATTERN:
	printf ("EqId %03d (rcu %d): WRITE_PATTERN command is being executed ...\n",
		rcu[i]->eqid, rcu_array[i]);
	if ( PEDCONF_RCU_FW_VERSION == 1 ) ret = writePattern_fw1(rcu[i], rcu_array[i]);
	if ( PEDCONF_RCU_FW_VERSION == 2 ) ret = writePattern_fw2(rcu[i], rcu_array[i]);
	break;
      case PEDCONF_WRITE_FPED:
	printf ("EqId %03d (rcu %d): WRITE_FPED command is being executed ...\n",
		rcu[i]->eqid, rcu_array[i]);
	ret = writeFPED(rcu[i], rcu_array[i]);
	break;
      case PEDCONF_WRITE_ZSTHR:
	printf ("EqId %03d (rcu %d): WRITE_ZSTHR command is being executed ...\n",
		rcu[i]->eqid, rcu_array[i]);
	ret = writeZSTHR(rcu[i], rcu_array[i]);
	break;
      case PEDCONF_WRITE_BCTHR:
	printf ("EqId %03d (rcu %d): WRITE_BCTHR command is being executed ...\n",
		rcu[i]->eqid, rcu_array[i]);
	ret = writeBCTHR(rcu[i], rcu_array[i]);
 	break;
      default:
	printf ("EqId %03d (rcu %d): No command is being executed ...\n",
		rcu[i]->eqid, rcu_array[i]);
      }
      
      /* report errors */
      if (ret != 0)
	printf("Command failed on %s for eqid %i, ret=%d\n", hostname, rcu[i]->eqid, ret);
      
      /*** child process ends here ***/
      exit(ret);
    }

    /*** parent process continues here ***/
    if (pid > 0) { 
      eqid_pid[i] = pid;
    }
    
  } // end equipment loop
  
  int ctr = 0;

  /* waiting for the child processes */
  for (i = 0; i < num_eqid; i++) {
    if ( eqid_array[i] == -1 ) continue;
    if (debug) printf("TPCpedConfig: waiting for child with pid %i on %s ... ", eqid_pid[i], hostname);
    waitpid (eqid_pid[i], &status, 0);
    if (debug) printf("ok\n");
    ctr++;
  }
  
  /* releasing the opened RORCs */
  for (i = 0; i < num_eqid; i++) {
    if ( eqid_array[i] == -1 ) continue;
    if ((rcu[i]->key != NULL) && (rcu[i]->key->err == 0)) {
      usleep (2000);
      release_channel (rcu[i]->key);
    }
  }
  
    /* deallocate the RCU models */
  for (i = 0; i < num_eqid; i++) {
    if ( eqid_array[i] == -1 ) continue;
    free_rcu_model (rcu[i]);
  }
  
  printf ("TPCpedConfig: Finished with execution on %d equipment(s) on %s.\n", ctr, hostname);

  return 0;
}

//EOF

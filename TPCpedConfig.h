
#ifndef TPCPEDCONFIG_H
#define TPCPEDCONFIG_H

#include <math.h>
#include <mysql.h>

#define DB_DATE_HOST    getenv("DATE_DB_MYSQL_HOST")
#define DB_DATE_USER    getenv("DATE_DB_MYSQL_USER")
#define DB_DATE_PASS    getenv("DATE_DB_MYSQL_PWD")
#define DB_DATE_DB      getenv("DATE_DB_MYSQL_DB")
#define DB_DATE_NULL    (DB_DATE_HOST==NULL || DB_DATE_USER==NULL || DB_DATE_PASS==NULL || DB_DATE_DB==NULL)
#define DB_DATE_PARAMS  DB_DATE_HOST, DB_DATE_USER, DB_DATE_PASS, DB_DATE_DB, 3306, NULL, 0

/* query size */
#define PEDCONF_QUERY_BUFFER_SIZE 8192

/* return codes */
#define DB_OK            0
#define DB_PARAMETERS   -1
#define DB_CONNECTION   -2
#define DB_QUERY        -3
#define DB_RETURN       -4
#define DB_EMPTY        -5

/* Internal commands */
#define PEDCONF_WRITE_RAMP        1
#define PEDCONF_WRITE_TESTPATTERN 2
#define PEDCONF_WRITE_PATTERN     3
#define PEDCONF_WRITE_FPED        4
#define PEDCONF_WRITE_ZSTHR       5
#define PEDCONF_WRITE_BCTHR       6

/* some commands and sizes of memories */
int PEDCONF_RCU_INSTRUCTION_MEM_ADDR;
int PEDCONF_RCU_PATTERN_MEM_ADDR;
int PEDCONF_RCU_INSTRUCTION_MEM_SIZE;
int PEDCONF_RCU_PATTERN_MEM_SIZE;
int PEDCONF_RCU_EXEC;
int PEDCONF_RCU_END;

/* RCU Firmware Version */
int PEDCONF_RCU_FW_VERSION;

const unsigned int PEDCONF_ALTRO_PATTERN_MEM_SIZE=1024;

// =================================================
bool debug    = false;
bool rounding = true;   // round or floor values ?
// =================================================

//
// 1) Misc functions
//

//========================================================================
void printhelp() {

  printf("=====================================================================|\n");
  printf("|TPCpedConfig: Run on LDC to write pedestal data or threshold values |\n");
  printf("|              as a function of measured noise into ALTROs           |\n");
  printf("| This program uses the fec2rorc library provided with Date          |\n");
  printf("| Author: Christian Lippmann, Christian.Lippmann@cern.ch             |\n");
  printf("|====================================================================|\n");
  printf("|Usage:   ./TPCpedConfig <options>                                   |\n");
  printf("|Examples: ./TPCpedConfig -fped data.dat -minor 4 -channel 0         |\n");
  printf("|          ./TPCpedConfig -pattern tpcPedestalMem.data               |\n");
  printf("| Specify either revision, serial and channel or minor and channel   |\n");
  printf("|========= Options: =================================================|\n");
  printf("|       -debug    = get debug output                                 |\n");
  printf("|       -fwv      = specify rcu firmware version (optional,default=2)|\n");
  printf("|      --- RORC Options ---------------------------------------------|\n");
  printf("|       -revision = specify rorc revision                            |\n");
  printf("|       -serial   = specify rorc serial                              |\n");
  printf("|       -minor    = specify minor number instead of revision&serial  |\n");
  printf("|       -channel  = specify rorc channel                             |\n");
  printf("|       -rcu      = specify rcu (0 ... 5)                            |\n");
  printf("|      --- Pedestal Memory (p(t)) Options ---------------------------|\n");
  printf("|       -ramp     = write test data to pedestal memory               |\n");
  printf("|       -pattern <file> = write to pedestal memory                   |\n");
  printf("|       -testpattern <occupancy> = write test pattern to ped. mem.   |\n");
  printf("|                                  (occupancy = [0...100] (percent)) |\n");
  printf("|      --- Pedestal (fixed) Options ---------------------------------|\n");
  printf("|       -fped <file> = write fixed pedestal values                   |\n");
  printf("|      --- Threshold Options ----------------------------------------|\n");
  printf("|       -bcthr <file> = write thresholds for moving average unit     |\n");
  printf("|       -zsthr <file> = write thresholds for zero suppression unit   |\n");
  printf("|       -offset <num> = specify offset (used for ZSTHR)              |\n");
  printf("|       -factor <num> = specify factor to multiply noise with        |\n");
  printf("|                       (used for ZSTHR and BCTHR, default=3)        |\n");
  printf("|       -min <num>    = minimum threshold (ZSTHR, BCTHR, default=2)  |\n");
  printf("|       -max <num>    = maximum threshold (ZSTHR, BCTHR, default=10) |\n");
  printf("|====================================================================|\n");

}


//========================================================================
int getPatchFromEquipmentID(int equipmentID) {
  // Get rcu (patch) index (0 ... 5) from equipment ID

  int retval = 0;

  if ( (equipmentID < (3<<8)) || (equipmentID > 983) ) {
    printf("Equipment ID (%d) outside range !", equipmentID);
    return -1;
  }
  if ( ( (int)equipmentID - 840 ) < 0) retval = (equipmentID-768)%2;
  else                                 retval = (equipmentID-840)%4 + 2;
  return retval;
}

//========================================================================
unsigned long roundtoint(double number) {
  return (unsigned long)(floor(number + 0.5));
}

//========================================================================
void filltestarray(unsigned long *pedestal_data, int occupancy, int offset) {
  //
  // Fill an array with random stuff
  //
  int i, j, pos;
  for ( i = 0; i < occupancy; i++ ) {
    pos = offset+10*(rand()%100);
    if ( pedestal_data[pos] < 1 ) {  // still empty
      for (j=0; j<10; j++) pedestal_data[pos+j] = 50;
    } else {                         // try again
      do {
        pos+=10;
        if ( pos >= (1000+offset) ) pos=offset;
      } while ( pedestal_data[pos] > 45 );
      for ( j=0; j<10; j++ ) pedestal_data[pos+j] = 50;
    }
  }
}

//========================================================================
float readfloat(int *ok, FILE *fp) {
  //
  // Function to read a float from a file
  //

  char string[1001];

  if ( fscanf(fp,"%s",string) == EOF ) {
    *ok = 0;
    return -1;
  }
  *ok = 1;
  return (float) atof(string);
}

//========================================================================
int readint(int *ok, FILE *fp) {
  //
  // Function to read an integer from a file
  //

  char string[1001];
  if ( fscanf(fp,"%s",string) == EOF ) {
    *ok = 0;
    return -1;
  }
  *ok = 1;
  return (int) atoi(string);
}


//
// 2) Functions converting hardware address
//

//========================================================================
int codeAddress(int Branch,  int FEC,  int chip, int channel) {
  return ((Branch&1)<<11) + ((FEC&0xf)<<7) + ((chip&0x7)<<4) + (channel&0xf);
}

//========================================================================
int decodedAddressBranch(int Address) {
  return ((Address>>11)&1);
}

//========================================================================
int decodedAddressFECaddr(int Address) {
  return ((Address>>7)&0xf);
}

//========================================================================
int decodedAddressChipaddr(int Address) {
  return ((Address>>4)&0x7);
}

//========================================================================
int decodedAddressChanneladdr(int Address) {
  return ((Address&0xf));
}

//
// 3) RCU model
//

//========================================================================
typedef struct {
  int id;                // database   
  int eqid;              // database
  chan_key* key;         // FEC2RORC
  int ddl_revision;      // FEC2RORC
  int ddl_serial;        // FEC2RORC
  int ddl_channel;       // FEC2RORC
  int afl;               // RCU register
  int trgcf;             // RCU register
  int trigger;           // HW/SW trigger
  int status;            // status of the configuration
  int pmem_retry_count;  // count IMEM restarts due to buggy -512 error
} rcu_model_t;

//========================================================================
rcu_model_t* get_new_rcu_model() {
  /*   create a new and empty RCU model  */
  rcu_model_t* temp = (rcu_model_t *)calloc(1, sizeof(rcu_model_t));  
  if (temp == NULL) return NULL;

  temp->pmem_retry_count = 0;
  temp->key = NULL;
  return temp;
}

//========================================================================
void free_rcu_model(rcu_model_t* rcu) {
  if (debug) printf("Freeing RCU model ... ");
  free(rcu);
  if (debug) printf("ok\n");
}

//
// 4) MySql db functions
//

//========================================================================
int get_eqid(int *eqid_array, int array_size) {

  /*   connect to DATE database
   *   query the eqIds for this LDC
   *   load the RCU model with eqid_array */

  char hostname[255];
  int count;
  int num_rows;
  
  MYSQL db;
  char query[PEDCONF_QUERY_BUFFER_SIZE];
  MYSQL_RES *res;
  MYSQL_ROW row;

  if (DB_DATE_NULL) return DB_PARAMETERS;

   mysql_init(&db);
   if (!mysql_real_connect(&db, DB_DATE_PARAMS)) {
    printf(mysql_error(&db));
    return DB_CONNECTION;
  } 

  /* get hostname of this LDC */
  if (gethostname(hostname, 255) != 0) {
    printf("TPCpedConfig: Error: Failing gethostname in get_eqid");
    mysql_close(&db);
    return -1;
  }

  /* query */
  memset(query, '\0', PEDCONF_QUERY_BUFFER_SIZE*sizeof(char));
  snprintf(query, PEDCONF_QUERY_BUFFER_SIZE,
	   "select EqId from EQUIP_PARAM_RorcData where "
	   "ldc in (select NAME from ROLES where HOSTNAME like '%s' and ROLE = 'LDC') "
	   "and ACTIVE = 1;", hostname);  
  if (mysql_real_query(&db,query,strlen(query))) {
    printf(mysql_error(&db));
    mysql_close(&db);
    return DB_QUERY;
  }
  
  /* result: several rows where [0] = eqId */
  res = mysql_store_result(&db);
  num_rows = mysql_num_rows(res);
  if (num_rows > array_size) {      // no copying of eqIds
    mysql_free_result(res);
    mysql_close(&db);
    return num_rows; 
  }
  row = mysql_fetch_row(res);
  for (count = 0; row != NULL; row = mysql_fetch_row(res)) {
    if (row[0] == NULL) {
      mysql_free_result(res);
      mysql_close(&db);
      return DB_RETURN;
    }
    eqid_array[count] = strtol(row[0], (char **)NULL, 10);
    count++;
  }

  mysql_free_result(res);
  mysql_close(&db);
  return count;
}

//========================================================================
int get_ddl_details(rcu_model_t* rcu) {

/*   connect to DATE database   
 *   query the RORC for this eqEd
 *   load the RCU model with ddl_revision, ddl_serial, ddl_channel */

  MYSQL db;
  char query[PEDCONF_QUERY_BUFFER_SIZE];
  MYSQL_RES *res;
  MYSQL_ROW row;

  if (DB_DATE_NULL) {
    return DB_PARAMETERS;
  }
  mysql_init(&db);
  if (!mysql_real_connect(&db, DB_DATE_PARAMS)) {
    printf(mysql_error(&db)); 
    return DB_CONNECTION;
  }
  
  /* query */
  memset(query, '\0', PEDCONF_QUERY_BUFFER_SIZE*sizeof(char));
  snprintf(query, PEDCONF_QUERY_BUFFER_SIZE, 
	   "select rorcRevision,rorcSerialNb,rorcChannelNb from EQUIP_PARAM_RorcData where EqId=%d;",
	   rcu->eqid);
  if (mysql_real_query(&db, query, strlen(query))) {
    printf(mysql_error(&db));
    mysql_close(&db);
    return DB_QUERY;
  }

  /* result: one row where [0] = revision, [1] = serial, [2] = channel */
  res = mysql_store_result(&db);
  row = mysql_fetch_row(res);
  if (row == NULL) {
    mysql_free_result(res);
    mysql_close(&db);
    return DB_EMPTY;
  } else {
    if ((row[0] == NULL) || (row[1] == NULL) || (row[2] == NULL)) {
      mysql_free_result(res);
      mysql_close(&db);
      return DB_RETURN;
    }
    rcu->ddl_revision = strtol(row[0], (char **)NULL, 10);
    rcu->ddl_serial   = strtol(row[1], (char **)NULL, 10);
    rcu->ddl_channel  = strtol(row[2], (char **)NULL, 10);
  }

  mysql_free_result(res);
  mysql_close(&db);
  return DB_OK;
}


//
// 5) RCU Fw V2 specific functions
//

//========================================================================
int RCU_SIB_Empty_Wait(rcu_model_t* rcu) {

  int sif_status = 0;
  int sif_errors  = 0;
  unsigned int sib_nempty = 1;
  unsigned int tcount = 0;

  int A_SIFSTATUS = 0x0;
  int A_SIFERRORS = 0x10000;
  int M_SIFSTATUS = 0x73f0; /* to be checked with Csaba */
  int M_SIFERRORS = 0x3f;   /* to be checked with Csaba */

  /* Checking if the SIF Input Buffer is empty */
  while(sib_nempty) {

    if (debug) printf("*Calling ddl_statusReadout(rcu->key, SIFSTATUS)\n");
    sif_status = ddl_statusReadout(rcu->key, A_SIFSTATUS);
    if (sif_status < 0) {
      printf("RCU_Exec_EXECSEQ() Error in ddl_statusReadout:0x%x\n", sif_status);
      return 1;
    }

    sib_nempty = (sif_status >> 4) & 0x1;
    if (sib_nempty) {
      sif_errors = ddl_statusReadout(rcu->key, A_SIFERRORS);
      if (debug) printf("RCU_Exec_EXECSEQ() Waiting for SIB to get empty SIFSTATUS=0x%x SIFERROR=0x%x\n",
			sif_status & M_SIFSTATUS, sif_errors & M_SIFERRORS);
    }

    if (debug) printf("RCU_Exec_EXECSEQ() SIF Status Register=%d\n", sif_status);

    if (tcount == 100){
      if (debug) printf("RCU_Exec_EXECSEQ() Access to SIB timeout (10 ms elapsed) SIB STATUS=0x%x SIF_ERRORS=0x%x TCOUNT=%d\n",
			sif_status, sif_errors, tcount);
      return 1;
    }

    usleep(2000);
    tcount++;

  } // end while

  if (debug) printf("RCU_Exec_EXECSEQ() SIB STATUS=0x%x SIF_ERRORS=0x%x TCOUNT=%d\n",
		    sif_status, sif_errors, tcount);

  return 0;
}

//========================================================================
int RCU_Exec_EXECSEQ(rcu_model_t* rcu) {

  int ret;

  /* Checking if the SIF Input Buffer is empty */
  if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
  ret = RCU_SIB_Empty_Wait(rcu);
  if (ret != 0) {
    if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
    return 1;
  }

  /* writing 1 word of data into the SIF Input Buffer (SIB) */
  if (debug) printf("Calling ddl_writeBlock()\n");
  //ret = ddl_sendCommand(rcu->key, PEDCONF_RCU_EXEC);
  unsigned long data = 0x0;
  ret = ddl_writeBlock(rcu->key, &data, 1, PEDCONF_RCU_EXEC);
  if (ret != 0) {
    if (debug) printf("\nError in ddl_writeBlock(): %i\n", ret);
    return 1;
  }

  /* Checking if the SIF Input Buffer is empty */
  if (debug) printf("Calling RCU_SIB_Empty_Wait()\n");
  ret = RCU_SIB_Empty_Wait(rcu);
  if (ret != 0) {
    if (debug) printf("Error in RCU_SIB_Empty_Wait(): %i\n", ret);
    return 1;
  }

  return 0;

}

#endif // TPCPEDCONFIG_H

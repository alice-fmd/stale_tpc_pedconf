MAINFILE=TPCpedConfig
SCRIPTNAME=TPC_PedestalConfiguration
PROJNAME=daq-TPC-configPedestal
VERSION=0.8
RELEASE=1
KIT:=$(PROJNAME)-${VERSION}

SRCS=Makefile $(MAINFILE).c $(MAINFILE).h $(SCRIPTNAME)_*.sh

CXXFLAGS=-Wall -O2 -pipe -fPIC -g

MYSQL_DIR=/usr/include/mysql
DATE_TPCC_DIR=/date/tpcc
DATE_TPCC_BIN=/date/tpcc/Linux
DATE_RORC_DIR=/date/rorc
DATE_RORC_BIN=/date/rorc/Linux
DATE_FEC_DIR=/date/fec
DATE_FEC_BIN=/date/fec/Linux

OBJS=${DATE_FEC_BIN}/fec2rorc_lib.o \
     ${DATE_RORC_BIN}/rorc_lib_p.o \
     ${DATE_RORC_BIN}/rorc_ddl_p.o \
     ${DATE_RORC_BIN}/rorc_aux.o

MYSQLLIBS := $(shell mysql_config --libs_r) -lz
MYSQLCOMP := $(shell mysql_config --cflags)

FILEO         = $(shell ls -q *.o)

# local temp directory
ifeq (${PWD},)
  PWD := ${shell pwd}
endif
TMPDIR := ${PWD}/tmp

# ===================================================================
default : $(MAINFILE)

# ======================= C File compiled with gcc ==================
%.o : %.c %.h
	gcc $(CXXFLAGS) -I$(DATE_RORC_DIR) -I$(DATE_FEC_DIR) -I$(MYSQL_DIR) -c $<
#	gcc $(CXXFLAGS) -I$(DATE_RORC_DIR) -I$(DATE_FEC_DIR) -I$(MYSQL_DIR) -I$(DATE_INFOLOGGER_DIR) -c $<
	@echo " ======================================================"

# ======================= CPP Files compiled with g++ ===============
%.o : %.C
	g++ $(CXXFLAGS) -c $<
	@echo " ======================================================"

# ======================= Classes and Programs ======================
TPCpedConfig : $(MAINFILE).o
	gcc $(CXXFLAGS) -o $(MAINFILE) $(MAINFILE).o ${MYSQLLIBS} $(OBJS)
	@echo " ======================================================"

dummy_patt : create_dummy_pattern_file.o
	g++ $(CXXFLAGS) -o create_dummy_pattern_file create_dummy_pattern_file.o
	@echo " ======================================================"

dummy_ped : create_dummy_ped_file.o
	g++ $(CXXFLAGS) -o create_dummy_ped_file create_dummy_ped_file.o
	@echo " ======================================================"

dummy_noise : create_dummy_noise_file.o
	g++ $(CXXFLAGS) -o create_dummy_noise_file create_dummy_noise_file.o
	@echo " ======================================================"

# ======================= Clean =====================================
clean :
	rm -rf *.o *~ $(MAINFILE) create_dummy_*_file *.data ${TMPDIR} $(PROJNAME)-*.tar.gz *.rpm
	@echo " ======================================================"

# ======================= Buid all ==================================
all : $(MAINFILE) dummy_ped dummy_noise dummy_patt

# ======================= make RPM ==================================
rpm :
	@rm -rf ${TMPDIR}/${KIT}
	@mkdir -p ${TMPDIR}/${KIT}
	@cp -p ${SRCS} ${TMPDIR}/${KIT}        
	@tar -cz -C ${TMPDIR} ${KIT} > ./${KIT}.src.tar.gz
	@echo " ===== Source package ${KIT}.src.tar.gz created:  ====="
	@tar -tzf ./${KIT}.src.tar.gz
	@echo " ======================================================"
	@rm -rf $(TMPDIR)/SOURCES $(TMPDIR)/SPECS $(TMPDIR)/BUILD $(TMPDIR)/RPMS $(TMPDIR)/SRPMS
	@mkdir -p $(TMPDIR)/SOURCES $(TMPDIR)/SPECS $(TMPDIR)/BUILD $(TMPDIR)/RPMS $(TMPDIR)/SRPMS
	@cp ./${KIT}.src.tar.gz $(TMPDIR)/SOURCES
	@rpmbuild --define "_topdir $(TMPDIR)" -ba $(KIT)-$(RELEASE).spec
	@find $(TMPDIR)/ -name "${KIT}-*.rpm" -exec cp -p {} . \;
	@echo
	@echo " ===== Rpms created! =================================="
	@echo " ======================================================"

